import Vue from "vue";
import VueRouter from "vue-router";

Vue.use(VueRouter);

function guardMyroute(to, from, next) {
  var isAuthenticated = false;
  if (window.localStorage.getItem("panel-token")) {
    isAuthenticated = true;
  } else {
    isAuthenticated = false;
  }
  if (isAuthenticated) {
    next();
  } else {
    next("/login");
  }
}

const routes = [
  {
    path: "/login",
    name: "login",
    component: () => import("../views/Login.vue"),
  },
  {
    path: "/registration",
    name: "registration",
    component: () => import("../views/Registration.vue"),
  },
  {
    path: "/articles/page/1",
    name: "articles",
    redirect: "/articles",
    component: () => import("../views/Articles.vue"),
  },
  {
    path: "/",
    name: "/layout",
    redirect: "/articles",
    beforeEnter: guardMyroute,
    component: () => import("@/layout/Layout.vue"),
    children: [
      {
        path: "/articles",
        name: "Articles",
        component: () => import("../views/Articles.vue"),
        children: [
          {
            path: "page/:page",
            name: "articlePages",
            component: () => import("../views/Articles.vue"),
          },
        ],
      },
      {
        path: "/articles/create",
        name: "create",
        component: () => import("../views/CreateArticle.vue"),
      },
      {
        path: "/articles/edit/:slug",
        name: "edit",
        component: () => import("../views/EditArticle.vue"),
      },
    ],
  },
];

const router = new VueRouter({
  mode: "history",
  base: process.env.BASE_URL,
  routes,
});

export default router;
