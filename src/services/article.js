import api from "./api";

const ArticleDataProvider = {
  create(newArticle) {
    return api.post("articles", {
      article: newArticle,
    });
  },
  getOne(slug) {
    return api.get("articles", slug);
  },
  getList(params = {}) {
    return api.query("articles", { params });
  },
  delete(slug) {
    return api.delete(`articles/${slug}`);
  },
  update(slug, params) {
    return api.update("articles", slug, { article: params });
  },
};

export default ArticleDataProvider;
