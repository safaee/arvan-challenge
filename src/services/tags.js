import api from "./api";

const TagDataProvider = {
  getList() {
    return api.query("tags");
  },
};

export default TagDataProvider;
