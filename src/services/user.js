import api from "./api";

const UserDataProvider = {
  create(newUser) {
    return api.post("users", { user: newUser });
  },
  getOne() {
    return api.get("user");
  },
  login(user) {
    return api.post("users/login", { user: user });
  },
};

export default UserDataProvider;
