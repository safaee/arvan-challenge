import moment from "moment";

export const formatArticle = (article) => {
  const { tagList, body, author, createdAt, slug, ...other } = article;
  return {
    ...other,
    excerpt: body.length <= 20 ? body : body.substr(0, 20) + " ...",
    tags: tagList.join(","),
    created: moment(createdAt).format("MMMM DD, YYYY"),
    author: author.username,
    slug: slug,
  };
};

export const articles = {
  namespaced: true,
  state: {
    articles: [],
    editingArticle: null,
    createStatus: false,
    editStatus: false,
    allArticles: 10,
    editError: false,
    editErrorMessage: "",
  },
  mutations: {
    addArticles(state, payload) {
      state.articles = payload;
    },
    deleteArticles(state, payload) {
      state.articles = state.articles.filter(
        (article) => article.slug !== payload
      );
    },
    setEditingArticle(state, payload) {
      state.editingArticle = payload;
    },
    setCreateStatus(state, payload) {
      state.createStatus = payload;
    },
    setEditStatus(state, payload) {
      state.editStatus = payload;
    },
    setAllArticles(state, payload) {
      state.allArticles = payload;
    },
    changeEditError(state, payload) {
      state.editError = payload;
    },
    setEditErrorMessage(state, payload) {
      state.editErrorMessage = payload;
    },
  },
  getters: {
    articles: (state) => {
      return state.articles.map((article, index) => {
        return {
          index: index + 1,
          ...formatArticle(article),
        };
      });
    },
  },
};
